# Frontend Web Engineer Experience

### Lead the delivery of coding while meeting quality criteria and project constraints:
- Led a team of web developers at XYZ Company, ensuring timely project delivery and adherence to coding standards.
- Established and enforced coding standards, conducting regular code reviews to maintain code quality.
- Utilized automated testing and continuous integration tools to ensure code quality and meet project deadlines.

### Explore and try different methods to improve productivity of the team:
- Championed a culture of continuous improvement, experimenting with Agile and Kanban methodologies.
- Implemented project management tools and refined development processes to enhance team productivity.

### Ensure alignment to Chief Architect roadmaps and strategies:
- Collaborated closely with the Chief Architect to align web development with architectural goals.
- Regularly reviewed and updated the development roadmap to support long-term company strategies.
- Conducted architecture review sessions to ensure project alignment with architectural vision.

### Meet service levels for systems (availability, security, and performance):
- Monitored system availability, security, and performance, implementing best practices and security measures.
- Conducted regular vulnerability assessments and implemented performance optimization strategies to maintain high service levels.

### Influence the direction of the overall architecture:
- Actively contributed to architectural discussions, influencing technology stack choices.
- Promoted the adoption of modern web technologies such as web components and progressive web apps to enhance user experience.

### Understands deeply about the modern web platform, especially web components, progressive web apps:
- Organized training sessions and workshops to ensure team understanding of web components and progressive web app concepts.
- Successfully implemented these technologies in our web projects to improve overall performance.

### Critical attention to detail about code, efficiency, and design:
- Maintained high code quality standards, conducting regular code reviews.
- Ensured code was efficient, maintainable, and well-designed.

### Strong problem-solving ability and strong analytical skills:
- Led troubleshooting efforts, root cause analysis, and debugging sessions to resolve complex technical issues.
- Identified and mitigated technical debt within the codebase.

### Develop and maintain standards of software development and components where applicable:
- Established and documented development standards and best practices for the team.
- Ensured code consistency and facilitated onboarding for new team members.

### Mentor & coach other developers in their learning & development:
- Actively mentored junior developers, providing guidance and support in their professional growth.
- Organized knowledge-sharing sessions and skill development opportunities.

### Be able to work alone or with others as needs dictate:
- Demonstrated flexibility in work style, adapting to both independent and collaborative tasks as required.

### Take ownership of all assigned tasks:
- Ensured timely completion and quality of all assigned tasks.

### Take ownership of systems and services assigned in production:
- Monitored and maintained the performance and availability of systems and services in production.
- Responded promptly to incidents to minimize downtime.

### Be proactive in promoting 'Best Practices':
- Promoted best practices within the team through workshops and documentation.

### Be available for out-of-hours support as required:
- Readily available for on-call support, responding to critical incidents promptly.

### Has experience working in an agile development lifecycle:
- Extensive experience in Agile environments, participating in sprint planning, daily stand-ups, and retrospectives.
- Advocated for Agile principles to enhance project flexibility and efficiency.

### Communication (written/verbal) to be well developed and of a professional standard:
- Maintained clear, concise, and professional communication in both written and verbal interactions.

### Can liaise with a broad range of people, including line management, senior management, Product owners, and related people:
- Regularly interacted with stakeholders at all levels of the organization, effectively communicating project status and technical details to ensure alignment with business goals.


# Knowledge and skillsets

### Experience with the latest versions of JavaScript (ES6/ESNext), HTML, CSS and NPM

1. **Question:** What is the significance of ES6 in JavaScript development?

   **Answer:** ES6 (ECMAScript 2015) introduced numerous new features and syntax improvements to JavaScript, making code more readable and maintainable. Key features include arrow functions, template literals, and the `let` and `const` declarations.

2. **Question:** How can you declare a constant variable in ES6?

   **Answer:** You can declare a constant variable in ES6 using the `const` keyword. For example:
   ```javascript
   const pi = 3.14159;
   ```

3. **Question:** What is the purpose of the spread operator (`...`) in JavaScript?

   **Answer:** The spread operator is used for various operations like copying arrays, combining arrays, and passing function arguments. It spreads the elements of an iterable (like an array) into another.

4. **Question:** What are template literals in JavaScript, and how are they different from regular strings?

   **Answer:** Template literals, denoted by backticks (\`), allow embedding expressions within strings using `${}`. They make string interpolation and multiline strings easier compared to regular strings enclosed in single or double quotes.

5. **Question:** What is the role of HTML5 in modern web development?

   **Answer:** HTML5 is the latest version of HTML and includes many new elements and APIs for multimedia, semantic markup, and improved compatibility with modern web technologies. It's crucial for building modern web applications.

6. **Question:** How can you include an external CSS file in an HTML document?

   **Answer:** To include an external CSS file in an HTML document, you use the `<link>` element in the document's `<head>` section. For example:
   ```html
   <link rel="stylesheet" type="text/css" href="styles.css">
   ```

7. **Question:** What is CSS Grid Layout, and how does it differ from Flexbox?

   **Answer:** CSS Grid Layout is a two-dimensional layout system that allows you to create grid-based designs. Flexbox, on the other hand, is a one-dimensional layout system primarily designed for arranging items in a single row or column. Grid is best for overall page layout, while Flexbox is ideal for item alignment within a container.

8. **Question:** What is the purpose of NPM (Node Package Manager) in web development?

   **Answer:** NPM is a package manager for JavaScript that simplifies the process of installing, managing, and sharing JavaScript packages and libraries. It's commonly used for dependency management in web development projects.

9. **Question:** How do you initialize a new Node.js project using NPM?

   **Answer:** To initialize a new Node.js project using NPM, you run the following command in your project's directory:
   ```
   npm init
   ```

10. **Question:** What is the difference between local and global dependencies in NPM?

    **Answer:** Local dependencies are specific to a particular project and are listed in the project's `package.json` file. Global dependencies are installed globally on your system and can be accessed by any project. Local dependencies are typically used for project-specific packages, while global dependencies are for utility tools and commands.

***
### W3C Standards and features

1. **Question:** What does W3C stand for, and what is its role in web development standards?

   **Answer:** W3C stands for the World Wide Web Consortium. Its role is to develop and maintain open standards to ensure the long-term growth and compatibility of the World Wide Web.

2. **Question:** What is HTML5, and how does it differ from previous versions of HTML?

   **Answer:** HTML5 is the fifth major revision of the HTML standard. It introduces new elements, attributes, and APIs for better structure, multimedia support, and improved semantics compared to previous versions.

3. **Question:** Explain the purpose of CSS3 in web development.

   **Answer:** CSS3 is the latest version of the Cascading Style Sheets standard. It introduces new features like gradients, transitions, animations, and flexible box layout, allowing for more advanced and visually appealing web designs.

4. **Question:** What is the role of the Document Object Model (DOM) in web development?

   **Answer:** The DOM is a programming interface for web documents. It represents the page's structure and content, allowing developers to manipulate and interact with HTML and XML documents using JavaScript or other programming languages.

5. **Question:** What is the significance of WAI-ARIA in web accessibility?

   **Answer:** WAI-ARIA (Web Accessibility Initiative - Accessible Rich Internet Applications) is a set of attributes that enhance the accessibility of web content for people with disabilities. It helps make web applications and dynamic content more usable for all users.

6. **Question:** How does WebRTC (Web Real-Time Communication) enhance web applications?

   **Answer:** WebRTC enables real-time communication in web applications by providing APIs for voice and video calling, peer-to-peer data sharing, and screen sharing without the need for plugins or third-party software.

7. **Question:** What is the purpose of WebSockets in web development?

   **Answer:** WebSockets is a protocol that enables bidirectional, real-time communication between a client (typically a web browser) and a server. It's used for applications that require low-latency and interactive data exchange, such as online gaming and chat applications.

8. **Question:** Explain the importance of responsive web design in the context of web development.

   **Answer:** Responsive web design ensures that web pages adapt and display correctly on various devices and screen sizes, providing a consistent user experience. This approach is crucial for mobile-friendly and accessible websites.

9. **Question:** What are Progressive Web Apps (PWAs), and why are they significant?

   **Answer:** PWAs are web applications that offer native app-like experiences, including offline functionality, push notifications, and fast loading. They provide a cross-platform solution and enhance user engagement.

10. **Question:** How does WebAssembly (Wasm) benefit web development?

    **Answer:** WebAssembly is a binary instruction format that allows running high-performance code written in languages like C, C++, and Rust in web browsers. It enables web applications to execute complex tasks more efficiently, making web development more powerful and versatile.

### Browser Compatibility and Modern Browser Features

1. **Question:** Why is browser compatibility important in web development?

   **Answer:** Browser compatibility ensures that websites and web applications function correctly and look consistent across different web browsers, providing a seamless user experience.

2. **Question:** What is a "user agent" in the context of web browsers?

   **Answer:** A user agent is a string sent by a web browser to a web server that identifies the browser and its version. It helps web servers deliver content tailored to the specific browser.

3. **Question:** How can you check the compatibility of a web feature across multiple browsers?

   **Answer:** Websites like Can I use (caniuse.com) provide compatibility tables and information about web features across various browsers, helping developers make informed decisions.

4. **Question:** What are vendor prefixes in CSS, and why were they used?

   **Answer:** Vendor prefixes (e.g., `-webkit-`, `-moz-`, `-ms-`, `-o-`) were used to implement experimental or non-standard CSS properties and features in different browsers. They were intended to provide early access to features but are now less commonly used due to standardized CSS.

5. **Question:** What is the purpose of polyfills in web development?

   **Answer:** Polyfills are scripts that provide modern functionality in older browsers that do not support certain features. They help bridge the gap between older and newer browser capabilities.

6. **Question:** What are some modern browser features that enhance web development?

   **Answer:** Modern browser features include support for HTML5, CSS3, WebSockets, WebAssembly, Service Workers, and improved JavaScript engines, which enable richer and more responsive web applications.

7. **Question:** How can you ensure responsive design in web development for different screen sizes and resolutions?

   **Answer:** Responsive design can be achieved using media queries in CSS to adapt layout and styling based on screen characteristics like width, height, and resolution.

8. **Question:** What are some strategies to optimize web performance and loading speed?

   **Answer:** Strategies for optimizing web performance include minimizing HTTP requests, leveraging browser caching, compressing assets, and using asynchronous loading of scripts.

9. **Question:** What is the importance of using modern, secure protocols like HTTPS in web development?

   **Answer:** HTTPS encrypts data exchanged between a user's browser and a web server, ensuring data privacy and security. It's essential for protecting sensitive information and gaining user trust.

10. **Question:** How does feature detection differ from browser detection, and which one is recommended in web development?

    **Answer:** Feature detection checks if a specific feature or API is supported by the browser, allowing developers to provide fallbacks or alternative functionality. Browser detection, on the other hand, identifies the browser and its version, which is less recommended due to its limitations and potential for inaccuracies.

### Web Accessibility

1. **Question:** What is web accessibility, and why is it important?

   **Answer:** Web accessibility refers to the practice of designing and developing websites and web applications that can be used by people with disabilities. It is essential to ensure that the web is inclusive and provides equal access to information and services for everyone.

2. **Question:** What is the Web Content Accessibility Guidelines (WCAG), and what does it provide?

   **Answer:** WCAG is a set of international guidelines for web accessibility. It provides a framework for making web content more accessible to individuals with disabilities by offering specific criteria and techniques for compliance.

3. **Question:** Name three categories of disabilities that web accessibility aims to address.

   **Answer:** Web accessibility aims to address visual disabilities (e.g., blindness), auditory disabilities (e.g., deafness), and motor disabilities (e.g., limited dexterity).

4. **Question:** What is alt text, and why is it important for images on websites?

   **Answer:** Alt text (alternative text) is a descriptive text attribute added to images in HTML. It provides a textual description of the image, which is read aloud by screen readers, making images accessible to visually impaired users.

5. **Question:** How can keyboard navigation be improved in web development for accessibility?

   **Answer:** To improve keyboard navigation, developers should ensure that all interactive elements can be accessed and activated using the keyboard alone. Additionally, focus styles should be visible and clearly indicate the currently focused element.

6. **Question:** What are ARIA roles and attributes, and how do they enhance web accessibility?

   **Answer:** ARIA (Accessible Rich Internet Applications) roles and attributes are used to enhance the accessibility of dynamic and interactive web content. They provide additional information to assistive technologies, making it easier for users with disabilities to navigate and interact with web applications.

7. **Question:** What is the purpose of semantic HTML elements in web accessibility?

   **Answer:** Semantic HTML elements (e.g., `<header>`, `<nav>`, `<main>`, `<button>`) have built-in meaning and help convey the structure and purpose of content to assistive technologies. Using semantic elements improves the overall accessibility and usability of web pages.

8. **Question:** How can developers test the accessibility of their websites and web applications?

   **Answer:** Developers can test accessibility using various tools and methods, including screen readers, browser developer tools, online accessibility checkers, and user testing with individuals with disabilities.

9. **Question:** What is the "contrast ratio," and why is it important in web accessibility?

   **Answer:** Contrast ratio measures the difference in luminance (brightness) between text and its background. It is essential for ensuring that text is readable, especially for users with low vision. WCAG provides specific contrast ratio requirements for text and background colors.

10. **Question:** How does responsive design relate to web accessibility?

    **Answer:** Responsive design, which adapts web content to different screen sizes and devices, can enhance web accessibility by ensuring that content remains usable and readable across a wide range of devices, including those used by individuals with disabilities.

### Internationalization and Localization

1. **Question:** What is the difference between internationalization (i18n) and localization (l10n)?

   **Answer:** Internationalization (i18n) is the process of designing and developing a product or service so that it can be adapted for different languages and cultures. Localization (l10n) is the process of adapting a product or service to a specific locale, including language, currency, and cultural conventions.

2. **Question:** Name some common elements that need to be localized in a software application.

   **Answer:** Elements that need localization include user interface text, date and time formats, currency symbols, number formats, and images with text.

3. **Question:** What is a "locale" in the context of localization?

   **Answer:** A locale is a set of preferences and settings that define a user's language, region, and cultural conventions. It includes information such as language code, country code, and character encoding.

4. **Question:** How does Unicode contribute to internationalization and localization?

   **Answer:** Unicode is a character encoding standard that provides a consistent way to represent text in different languages and scripts. It allows software to handle diverse languages and characters, making internationalization and localization easier.

5. **Question:** What is the purpose of "language fallback" in localization?

   **Answer:** Language fallback is a strategy used in localization to ensure that if a specific translation is not available for a language or region, a more general translation (e.g., for a closely related language) is used as a fallback to provide a better user experience.

6. **Question:** Explain the concept of "right-to-left" (RTL) text and its relevance in localization.

   **Answer:** RTL text is a writing direction used in languages like Arabic and Hebrew, where text flows from right to left. Localization must support RTL layouts and text rendering for languages that use this direction.

7. **Question:** What is the purpose of localization resource files in software development?

   **Answer:** Localization resource files store translated text and cultural settings separately from the code. They allow developers to manage and update translations without modifying the application's source code.

8. **Question:** How can developers make their web applications more internationalization-friendly?

   **Answer:** Developers can use libraries and frameworks that support i18n, separate text from code, and provide support for dynamic language switching to make their web applications more internationalization-friendly.

9. **Question:** Name a widely used framework or library for internationalization in JavaScript.

   **Answer:** One widely used JavaScript library for i18n is "i18next," which provides tools for handling translations, pluralization, and date formatting.

10. **Question:** What are some challenges in maintaining multilingual websites over time?

    **Answer:** Challenges in maintaining multilingual websites include keeping translations up-to-date, ensuring consistent terminology across languages, handling changes in content structure, and addressing issues related to dynamically generated content.

### Web Servers, Node.js (Express), and NGINX

1. **Question:** What is the primary function of a web server?

   **Answer:** A web server's primary function is to receive, process, and serve web requests from clients (usually web browsers) by delivering web content such as HTML files, images, and other resources.

2. **Question:** Explain the role of an HTTP server in web communication.

   **Answer:** An HTTP server is responsible for handling HTTP requests from clients and returning appropriate responses. It processes client requests by mapping URLs to resources and executing code or serving static files as needed.

3. **Question:** What is Node.js, and how does it differ from traditional web servers like Apache or Nginx?

   **Answer:** Node.js is a JavaScript runtime that allows server-side JavaScript execution. Unlike traditional servers like Apache or Nginx, Node.js is event-driven and non-blocking, making it suitable for building highly scalable and real-time applications.

4. **Question:** What is Express.js, and why is it commonly used with Node.js?

   **Answer:** Express.js is a web application framework for Node.js. It simplifies the process of building web applications and APIs by providing a set of robust features and middleware, making it a popular choice for Node.js developers.

5. **Question:** How can you create a simple HTTP server using Node.js and Express?

   **Answer:** You can create a simple HTTP server using Express as follows:

   ```javascript
   const express = require('express');
   const app = express();
   
   app.get('/', (req, res) => {
     res.send('Hello, World!');
   });
   
   app.listen(3000, () => {
     console.log('Server is running on port 3000');
   });
   ```

6. **Question:** Explain the purpose of NGINX in web server architecture.

   **Answer:** NGINX is a high-performance, open-source web server and reverse proxy server. It is commonly used for load balancing, caching, SSL termination, and serving as a front-end proxy to other web application servers.

7. **Question:** What is load balancing, and how does NGINX support it?

   **Answer:** Load balancing is the process of distributing incoming web traffic across multiple server instances to improve performance and reliability. NGINX supports load balancing by evenly distributing requests among a group of backend servers.

8. **Question:** How can NGINX be used as a reverse proxy server?

   **Answer:** NGINX can act as a reverse proxy by accepting client requests and forwarding them to backend servers. This allows NGINX to handle tasks like SSL termination, load balancing, and serving static files before requests reach the application server.

9. **Question:** What is the difference between NGINX and Apache as web servers?

   **Answer:** NGINX and Apache are both web servers, but they have different architectures. NGINX is known for its performance and efficiency, making it suitable for high-traffic websites. Apache is more feature-rich and versatile but may be less performant in certain scenarios.

10. **Question:** What is the benefit of using NGINX as a caching server in a web application stack?

    **Answer:** NGINX can cache static content and HTTP responses, reducing the load on backend servers and improving website performance. It serves cached content to clients, reducing response times and server load.


### Continuous Integration/Tooling, Git/Gitflow, Branching Strategies, Test Automation, Code Quality Tools, Code Scaffolding, Build Tools, and Related Libraries

1. **Question:** What is continuous integration (CI) in software development, and why is it important?

   **Answer:** Continuous Integration is a development practice where code changes are automatically built, tested, and integrated into the main codebase frequently. It helps detect and address integration issues early and ensures code quality and consistency.

2. **Question:** Explain the concept of Gitflow in version control.

   **Answer:** Gitflow is a branching model that defines specific branches for features, releases, and hotfixes in a Git repository. It provides a structured approach to managing code changes and releases in collaborative development environments.

3. **Question:** Name two commonly used branching strategies in Git other than Gitflow.

   **Answer:** Two commonly used branching strategies are "Feature Branching" (where each feature or task has its own branch) and "GitHub Flow" (a simplified approach for continuous delivery using branches).

4. **Question:** What is the purpose of automated testing in the software development process?

   **Answer:** Automated testing helps identify issues and bugs in software quickly, ensures code reliability, and provides confidence in making changes or adding new features without breaking existing functionality.

5. **Question:** Name two types of automated tests commonly used in software development.

   **Answer:** Two common types of automated tests are "unit tests" (testing individual components or functions) and "integration tests" (testing how different parts of the system work together).

6. **Question:** What are code quality tools, and how do they benefit developers?

   **Answer:** Code quality tools analyze code for issues such as code style violations, code smells, and potential bugs. They help developers maintain consistent coding standards and improve code readability and maintainability.

7. **Question:** What is code scaffolding in web development, and why is it useful?

   **Answer:** Code scaffolding is a code generation technique that automatically generates template code or project structures. It is useful for quickly setting up new projects or generating repetitive code patterns.

8. **Question:** Name two popular build tools used in modern web development.

   **Answer:** Two popular build tools are "Webpack" (for bundling and optimizing JavaScript and assets) and "Rollup" (focused on creating efficient JavaScript bundles).

9. **Question:** What is transpiling in the context of JavaScript development?

   **Answer:** Transpiling is the process of converting code written in one version of JavaScript (e.g., ES6) into an older version (e.g., ES5) that is compatible with older browsers and environments.

10. **Question:** How does Continuous Integration (CI) work with code repositories like GitHub or GitLab?

    **Answer:** CI services like GitHub Actions or GitLab CI/CD automatically build and test code changes whenever a new commit or pull request is created. This helps ensure that code changes do not introduce errors or regressions before they are merged into the main codebase.

###  PaaS and Web Engineering, Proxies, CDN and Caching, Gateways, HTTP Protocols and Browser Networking, Web Analytics and Logging

1. **Question:** What does PaaS stand for in the context of web engineering, and what is its significance?

   **Answer:** PaaS stands for Platform as a Service. It provides a cloud-based platform that offers development and deployment tools, databases, and infrastructure, simplifying the process of building, deploying, and scaling web applications.

2. **Question:** How do reverse proxies contribute to web engineering, and what are some popular reverse proxy servers?

   **Answer:** Reverse proxies act as intermediaries between clients and web servers. They can handle load balancing, SSL termination, and caching. Popular reverse proxy servers include NGINX and Apache HTTP Server.

3. **Question:** What is the role of a Content Delivery Network (CDN) in web engineering, and how does it improve website performance?

   **Answer:** A CDN is a network of distributed servers that deliver web content to users from geographically closer locations. It improves website performance by reducing latency, load times, and bandwidth consumption.

4. **Question:** Explain the concept of caching in web engineering and name two types of caching commonly used.

   **Answer:** Caching involves storing frequently accessed data temporarily to reduce the need to fetch it repeatedly. Two common types are "browser caching" (storing assets locally on the user's device) and "server-side caching" (storing pre-rendered pages or responses on the server).

5. **Question:** What is the role of API gateways in web engineering, and how do they simplify API management?

   **Answer:** API gateways serve as an entry point for APIs, handling tasks like request routing, security, authentication, rate limiting, and response transformation. They simplify API management by centralizing these functions.

6. **Question:** Name two widely used HTTP protocols in web engineering and explain the differences between them.

   **Answer:** Two widely used HTTP protocols are HTTP/1.1 and HTTP/2. HTTP/2 is a newer version that offers improvements in performance, multiplexing, and header compression compared to HTTP/1.1.

7. **Question:** What is the purpose of browser networking in web development?

   **Answer:** Browser networking involves the communication between a web browser and web servers over the internet. It handles tasks like DNS resolution, TCP connections, HTTP requests, and resource loading.

8. **Question:** How do web analytics tools help businesses in web engineering?

   **Answer:** Web analytics tools collect and analyze data about website traffic, user behavior, and conversions. They help businesses make informed decisions, optimize user experiences, and track the success of marketing efforts.

9. **Question:** What are server logs, and why are they important in web engineering?

   **Answer:** Server logs are records of server activity, including requests, errors, and other events. They are important for diagnosing issues, monitoring server health, and analyzing website performance.

10. **Question:** Explain the concept of server-side rendering (SSR) and its relevance in web engineering.

    **Answer:** Server-side rendering (SSR) involves rendering web pages on the server before sending them to the client's browser. It can improve SEO, performance, and the initial page load time by delivering pre-rendered content.

### Security, JWT/SAML, and API Security

1. **Question:** Why is security a critical consideration in web development?

   **Answer:** Security is essential to protect sensitive data, prevent unauthorized access, and ensure the integrity and availability of web applications. Neglecting security can lead to data breaches and other serious vulnerabilities.

2. **Question:** What does JWT stand for, and how is it used in web security?

   **Answer:** JWT stands for JSON Web Token. It is a compact, self-contained way to represent information securely between parties. JWTs are commonly used for authentication and authorization in web applications.

3. **Question:** Describe the purpose of SAML in web security.

   **Answer:** SAML (Security Assertion Markup Language) is an XML-based standard for exchanging authentication and authorization data between parties, primarily used for single sign-on (SSO) and identity federation in web applications.

4. **Question:** What is the fundamental difference between JWT and SAML in terms of token-based authentication?

   **Answer:** The fundamental difference is the format and use of tokens. JWT is typically used for stateless authentication, while SAML is used for identity federation and single sign-on across multiple domains or services.

5. **Question:** What are some common security threats in API security?

   **Answer:** Common API security threats include unauthorized access, injection attacks (e.g., SQL injection or XSS), denial of service attacks, and data exposure through inadequate authentication or authorization.

6. **Question:** How can you secure an API against unauthorized access?

   **Answer:** To secure an API against unauthorized access, you can implement authentication mechanisms like API keys, OAuth, or JWT tokens. Additionally, enforce proper authorization to restrict access to authorized users or roles.

7. **Question:** What is OAuth, and how does it enhance API security?

   **Answer:** OAuth is an open standard for access delegation and authorization. It allows third-party applications to access resources on behalf of a user without exposing user credentials. OAuth enhances API security by enabling controlled and secure access to resources.

8. **Question:** What is the principle of least privilege in API security, and why is it important?

   **Answer:** The principle of least privilege means granting the minimum level of access or permissions necessary for a user or application to perform its tasks. It is crucial to minimize the potential impact of a security breach by limiting access to only what is needed.

9. **Question:** How does rate limiting contribute to API security?

   **Answer:** Rate limiting restricts the number of requests a user or application can make within a specified timeframe. It helps prevent abuse, brute force attacks, and excessive usage of API resources.

10. **Question:** What is Cross-Origin Resource Sharing (CORS), and why should it be configured carefully in API security?

    **Answer:** CORS is a security feature that controls which origins (domains) can access resources on a web page or API. It should be configured carefully to avoid allowing unintended domains to make requests to your API, which can lead to security vulnerabilities.

### Architecture, Large-Scale Client-Side Web Applications, Atomic Design, SPAs, Progressive Web Apps, Micro-Frontend Design, State Management

Here are 10 questions about web architecture, large-scale client-side web applications, atomic design, SPAs (Single Page Applications), progressive web apps, micro-frontend design, and state management, along with their answers in markdown format:

1. **Question:** What is the significance of architectural design in web development?

   **Answer:** Architectural design defines the structure and organization of a web application. It helps ensure scalability, maintainability, and performance as the application grows.

2. **Question:** Describe the challenges of developing large-scale client-side web applications.

   **Answer:** Challenges in large-scale client-side applications include managing complex codebases, optimizing performance, handling state management, and ensuring a consistent user experience.

3. **Question:** What is Atomic Design, and how does it contribute to building scalable user interfaces?

   **Answer:** Atomic Design is a methodology for creating design systems by breaking UI components into smaller, reusable parts called atoms (e.g., buttons, input fields). It promotes consistency and scalability in UI development.

4. **Question:** Explain the concept of Single Page Applications (SPAs) and their advantages.

   **Answer:** SPAs are web applications that load a single HTML page and dynamically update content as the user interacts. They offer a smooth, app-like user experience, reduce page load times, and minimize server requests.

5. **Question:** What are Progressive Web Apps (PWAs), and how do they enhance web applications?

   **Answer:** PWAs are web applications that provide a native app-like experience, including offline functionality, push notifications, and fast loading. They enhance web applications by improving user engagement and performance.

6. **Question:** What is micro-frontend design, and why is it gaining popularity?

   **Answer:** Micro-frontend design involves breaking down a web application's frontend into smaller, independently deployable modules. It is gaining popularity because it allows teams to work on and deploy parts of the application independently, improving development speed and maintainability.

7. **Question:** Why is state management crucial in modern web applications, and what are some popular state management libraries in JavaScript?

   **Answer:** State management is crucial because it helps maintain the application's data and UI consistency. Popular state management libraries in JavaScript include Redux, MobX, and VueX.

8. **Question:** How does the Flux architecture pattern contribute to state management in web applications?

   **Answer:** Flux is a design pattern that manages application state by enforcing a unidirectional data flow. It helps maintain a predictable state and simplifies debugging in large-scale applications.

9. **Question:** What are the main differences between server-side rendering (SSR) and client-side rendering (CSR) in web architecture?

   **Answer:** SSR generates HTML on the server and sends it to the client, resulting in faster initial page loads and better SEO. CSR loads an empty shell and fetches data from the server, providing a smoother, more interactive experience.

10. **Question:** Name a popular JavaScript library for building micro-frontends and explain its purpose.

    **Answer:** Module Federation is a feature in Webpack 5 that allows developers to build micro-frontends. It enables the composition of independently developed parts into a cohesive application, making it easier to maintain and scale.

# CSS GRID

**Problem Statement:**

You are asked to create a simple webpage layout using HTML and CSS Grid Layout. The layout should have the following specifications:

1. Create a container div with three columns: one on the left, one in the center, and one on the right. The center column should be wider than the other two.

2. Define grid areas within the container div to arrange the sections of the webpage as follows:
   - The "navbar" section should span the entire top row.
   - The "hero" section should also span the top row, just below the "navbar."
   - The "sidebar" section should be in the left column, and its height should adjust automatically to not be less than 100px.
   - The "content" section should be in the center column and should have the same height as the "sidebar."
   - The "footer" section should span the entire bottom row and should be centered horizontally within the grid.

**HTML and CSS Code:**

```html
<!DOCTYPE html>
<html>
<head>
    <style>
        /* Define a container with a CSS Grid layout */
        .container {
            display: grid;
            grid-template-columns: 1fr 3fr 1fr; /* Three columns: 1/5, 3/5, 1/5 of the width */
            grid-template-areas: 
                "navbar navbar navbar" /* The navbar spans the top row */
                "hero hero hero"       /* The hero section also spans the top row */
                "sidebar content content" /* Sidebar and content share the same row */
                ". footer .";          /* An empty row to create space below the footer */
        }

        /* Style for the navbar section */
        .navbar {
            grid-area: navbar; /* Assign this element to the "navbar" grid area */
            background-color: #333; /* Background color */
            height: 50px; /* Fixed height of 50 pixels */
        }

        /* Style for the hero section */
        .hero {
            grid-area: hero; /* Assign this element to the "hero" grid area */
            background-color: #007bff; /* Background color */
            height: 80px; /* Fixed height of 80 pixels */
        }

        /* Style for the sidebar section */
        .sidebar {
            grid-area: sidebar; /* Assign this element to the "sidebar" grid area */
            background-color: #eee; /* Background color */
            min-height: 100px; /* Minimum height of 100 pixels; adjusts automatically if more content is present */
        }

        /* Style for the content section */
        .content {
            grid-area: content; /* Assign this element to the "content" grid area */
            background-color: pink; /* Background color */
            min-height: 100px; /* Minimum height of 100 pixels; same as the sidebar */
        }

        /* Style for the footer section */
        .footer {
            grid-area: footer; /* Assign this element to the "footer" grid area */
            background-color: #333; /* Background color */
            height: 75px; /* Fixed height of 75 pixels */
        }
    </style>
</head>
<body>
    <!-- The main container div that contains all the sections -->
    <div class="container">
        <!-- The navbar section -->
        <div class="navbar"></div>
        <!-- The hero section -->
        <div class="hero"></div>
        <!-- The sidebar section -->
        <div class="sidebar"></div>
        <!-- The content section -->
        <div class="content"></div>
        <!-- The footer section -->
        <div class="footer"></div>
    </div>
</body>
</html>
```



# PROMISE

### Problem Statement

You are given an initial function named `promisify`, which receives two arguments: `fn` (type: Function) and `hasCallback` (type: Boolean, default: true). The original function returns another function that receives the `args` object (containing the arguments) from the parent function.

**Requirements:**

The `promisify` function needs to be updated to satisfy the following requirements:

1. It must convert the function passed in the parameter `fn` into a Promise object (thus it can resolve/reject).
2. When the `hasCallback` parameter is true, `fn` is a function implementing the callback pattern (error, response).
3. When the `hasCallback` parameter is false, `fn` is a regular function.
4. If a non-function type is passed as the `fn` argument, "invalid" should be returned.
5. When a function is promisified, it can receive any number of arguments.

**Available packages/libraries:**

- Node.js v18.9.0

**Examples:**

1. **Example 1: Asynchronous function**

   ```javascript
   const fs = require('fs');
   const promisified = promisify(fs.readdir);

   promisified('./')
     .then((dirs) => {
       // Must resolve
     })
     .catch((err) => {
       // Must NOT reject
     });
   ```

2. **Example 2: Asynchronous function**

   ```javascript
   const fs = require('fs');
   const promisified = promisify(fs.readdir);

   promisified('./', 'utf8')
     .then((dirs) => {
       // Must resolve
     })
     .catch((err) => {
       // Must NOT reject
     });
   ```

3. **Example 3: Asynchronous function (failing)**

   ```javascript
   const fs = require('fs');
   const promisified = promisify(fs.unlink);

   promisified('/nonexistent.txt')
     .then((dirs) => {
       // Must NOT resolve
     })
     .catch((err) => {
       // Must fail (reject)
     });
   ```

4. **Example 4: Synchronous function**

   ```javascript
   function syncFn() {
     return 1;
   }

   const promisified = promisify(syncFn, false);

   promisified()
     .then((dirs) => {
       // Must resolve
     })
     .catch((err) => {
       // Must not reject
     });
   ```

### Solution

```javascript
function promisify(fn, hasCallback = true) {
 
  if (typeof fn !== "function") {
    return "invalid";
  }

  return function (...args) {
    return new Promise((resolve, reject) => {
      if (hasCallback) {
        fn(...args, (error, response) => {
          if (error) {
            reject(error);
          } else {
            resolve(response);
          }
        });
      } else {
        try {
          const result = fn(...args);
          resolve(result);
        } catch (error) {
          reject(error);
        }
      }
    });
  };
}

// Examples and usage are provided in the problem statement.
```

# PROXY

### Problem Statement

Your task is to implement a function that adds type validation to an object. The function should receive an object as its only argument and return an object with the same properties, but with type validation added. Types should be validated when:

- The function creates the object.
- Someone updates a property.
- Someone adds a property.

The type validation should always be based on the last part of the property name. For example, the `age_int` property should always be an integer and throw an error when set to something else.

Here are possible types:

- `string`: For example, "string type".
- `int`: 12.00 and 12 are both integers.
- `float`: For example, 12.34.
- `number`: Any int or float.
- `bool`: For example, true.

Assumptions:

- Types are optional, and validation should be skipped if the type isn't specified.

### Solution

```javascript
function typeCheck(obj) {
  const validatedObject = {};

  const typeValidators = {
    string: (value) => typeof value === 'string',
    int: (value) => Number.isInteger(value),
    float: (value) => typeof value === 'number' && isFinite(value) && !Number.isInteger(value),
    bool: (value) => typeof value === 'boolean',
  };

  const validateProperty = (prop, value) => {
    const propTypeMatch = prop.match(/_(\w+)$/);

   // Breakdown of the regular expression:
   // - /: Delimiter marking the beginning and end of the regular expression pattern.
   // - _: Matches the underscore character literally.
   // - (\w+): A capturing group that matches one or more word characters (\w).
   //           Word characters include letters, digits, and underscores.
   //           The capturing group allows us to extract the matched type suffix.
   // - $: Anchor that matches the end of the string.

    if (propTypeMatch) {
      const propType = propTypeMatch[1];
      const validate = typeValidators[propType];

      if (validate && !validate(value)) {
        throw new Error(`${prop} must be of type ${propType}.`);
      }
    }
  };

  for (const prop in obj) {
    validateProperty(prop, obj[prop]);
    validatedObject[prop] = obj[prop];
  }

  return new Proxy(validatedObject, {
    get(target, prop) {
      return target[prop];
    },
    set(target, prop, value) {
      validateProperty(prop, value);
      target[prop] = value;
      return true;
    },
  });
}

// Example usage:
const obj = {
  age_int: 2,
  name_string: "Adam",
  job: null,
  salary_float: 1500.5,
  address: "123 Main St",
};

// Apply type validation to the object using the typeCheck function.
const validatingObject = typeCheck(obj);

validatingObject.age_int = 2.25; // Throws error
validatingObject.age_int = 2;
validatingObject.job = "fireman";
validatingObject.address_string = 20; // Throws error

validatingObject.salary_float = "1500.5"; // Throws error
```
